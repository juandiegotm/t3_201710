package view;

import java.util.ArrayList;
import java.util.Scanner;

import controller.Controller;
import model.data_structures.Stack;

public class VistaEvaluadorDeExpresiones {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		String expresion = "";
		boolean fin = false;
		while(!fin){
			printMenu();

			int option = sc.nextInt();
			switch (option) {
			case 1:
				System.out.println("Ingresa la expresi�n aritmetica: ");
				expresion = sc.next();
				break;

			case 2:
				String respuesta = Controller.expresionBienFormada(expresion)?"s�": "no";
				System.out.println("La expresi�n " + expresion + " " + respuesta + " est�"+ " bien formada" );
				break;


			case 3:
				System.out.println("La expresi�n fue ordenada: ");
				Stack<Character> pilaOrdenada = Controller.ordenarPila(expresion);
				while(!pilaOrdenada.isEmpty())
					System.out.println(pilaOrdenada.pop());
				break;
				
			case 4:
				fin = true;
				break;
			}	
		}
	}
	private static void printMenu() {
		System.out.println("---------ISIS 1206 - Estructuras de datos----------");
		System.out.println("---------------------Taller 3----------------------");
		System.out.println("1. Ingresa una nueva expresion");
		System.out.println("2. Evaluar si es una expresion valida");
		System.out.println("3. Ordenar en una pila la expresi�n");
		System.out.println("4. Salir");
		System.out.println("Type the option number for the task, then press enter: (e.g., 1):");

	}
}
