package controller;

import model.data_structures.Stack;
import model.logic.EvaluadorExpresiones;

public class Controller {

	private static EvaluadorExpresiones evaluador = new EvaluadorExpresiones();
	
	public static boolean expresionBienFormada(String pExpresion){
		return evaluador.expresionBienFormada(pExpresion);
	}
	
	public static Stack<Character>ordenarPila(String pExpresion){
		return evaluador.ordenarPila(evaluador.pasarAPilaDeCaracteres(pExpresion));
	}
}
