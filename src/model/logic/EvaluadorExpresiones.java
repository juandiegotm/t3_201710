package model.logic;

import model.data_structures.*;

public class EvaluadorExpresiones {

	private Queue<String> fila;


	public boolean expresionBienFormada (String expresion){
		boolean estaBienFormada = true;
		char expresionEnArreglo[] = expresion.toCharArray();
		Stack<Character> pila = new Stack<>();
		for(int i = 0; i < expresionEnArreglo.length; i++){
			Character caracterActual = (Character) expresionEnArreglo[i];
			Character caracterAnterior =  i-1 >= 0 ? expresionEnArreglo[i-1]: 0;
			Character caracterSiguiente =i+1 < expresionEnArreglo.length ? expresionEnArreglo[i+1]: 0;

			if(caracterActual == '(' || caracterActual == '['){
				if(caracterSiguiente == ')' || caracterActual == ']')
					return false;
				else
					pila.push(caracterActual);
			}

			else if(caracterActual == ')'){
				if(pila.pop() != '(')
					return false;
			}
			else if(caracterActual == ']'){
				if(pila.pop() != '[')
					return false;
			}

			else if(Character.isDigit(caracterActual)){
				if(caracterSiguiente == '(' || caracterSiguiente == '[' )
					return false;
			}

			else if(caracterActual == '+' || caracterActual == '*' || caracterActual == '/' || caracterActual == '-'){
				if(caracterSiguiente == null || caracterAnterior == null || 
						caracterSiguiente == '+' || caracterSiguiente == '*' || 
						caracterSiguiente == '/' || caracterSiguiente== '-'  ){
					return false;
				}
			}

		}

		if(!pila.isEmpty()){
			estaBienFormada = false;
		}
		return estaBienFormada;
	}

	public Stack<Character> pasarAPilaDeCaracteres(String expresionBienFormada){
		Stack<Character> pilaDeCaracteres = new Stack<>();
		for(int i = 0; i < expresionBienFormada.length(); i++)
			pilaDeCaracteres.push(expresionBienFormada.charAt(i));

		return pilaDeCaracteres;


	}

	public Stack<Character> ordenarPila(Stack<Character> pilaExpresion){
		int cantidadOp = 0;
		int cantidadNum = 0;
		int cantidadParentesis = 0;

		Queue<Character> filaAux = new Queue<>();
		while(!pilaExpresion.isEmpty()){

			Character caracterActual = pilaExpresion.pop();
			if(Character.isDigit(caracterActual))
				cantidadNum++;
			else if(caracterActual == '(' || caracterActual == ')' || caracterActual == '[' || caracterActual == ']')
				cantidadParentesis++;
			else
				cantidadOp++;

			filaAux.enqueue(caracterActual);
		}


		int opLogrados = 0;
		int numLogrados = 0;
		int parentesisLogrados = 0;

		while(!filaAux.isEmpty()){
			Character caracterActual = filaAux.dequeue();
			if(numLogrados < cantidadNum){
				if(Character.isDigit(caracterActual)){
					pilaExpresion.push(caracterActual);
					numLogrados++;
				}
				else
					filaAux.enqueue(caracterActual);
			}

			else if(opLogrados < cantidadOp){
				if(caracterActual == '+' || caracterActual == '*' || caracterActual == '/' || caracterActual == '-'){
					pilaExpresion.push(caracterActual);
					opLogrados++;
				}

				else
					filaAux.enqueue(caracterActual);

			}

			else if(parentesisLogrados < cantidadParentesis){
				if(caracterActual == '(' || caracterActual == ')' || caracterActual == '[' || caracterActual == ']'){
					pilaExpresion.push(caracterActual);
					parentesisLogrados++;
				}

				else
					filaAux.enqueue(caracterActual);
			}
		}

		return pilaExpresion;

	}

}
